USE `gaia`;
-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: 127.0.0.1    Database: gaia
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `fazemos`
--

DROP TABLE IF EXISTS `fazemos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fazemos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `slug` varchar(140) DEFAULT NULL,
  `texto` text,
  `secao` varchar(140) DEFAULT NULL,
  `secao_slug` varchar(140) DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fazemos`
--

LOCK TABLES `fazemos` WRITE;
/*!40000 ALTER TABLE `fazemos` DISABLE KEYS */;
INSERT INTO `fazemos` VALUES (1,'Diagnóstico Organizacional','diagnostico_organizacional','<p>Desenvolvimento de Diagn&oacute;stico Organizacional de forma sistematizada, a partir do conjunto das vis&otilde;es obtidas por meio de olhar anal&iacute;tico de um especialista externo, observadas por &acirc;ngulos de hierarquia e das diferentes fun&ccedil;&otilde;es organizacionais.</p>','Desenvolvimento Organizacional','desenvolvimento_organizacional',0),(2,'Pesquisa de Clima e Cultura','pesquisa_de_clima_e_cultura','<p>Desenvolvimento e aplicação de metodologias que permitam identificar, analisar e propor ações que contribuam para melhoria e evolução do ambiente de trabalho e da Cultura Organizacional.</p>','Desenvolvimento Organizacional','desenvolvimento_organizacional',1),(3,'Gestão por Competência','gestao_por_competencia','<p>Planejamento e implementação de Mapeamento de Competências e Modelo de Desenvolvimento de Competências, essenciais para que as diferentes áreas da empresa estejam em sinergia com a cultura organizacional. O programa envolve a construção, em parceria, de processos integrados de recrutamento e seleção, avaliação de desempenho, matriz de bônus e programa de desenvolvimento. <br>» Desenvolvimento consciente | » Team Building | » Criatividade e Autonomia | » Processo de Decisão</p>','Desenvolvimento Organizacional','desenvolvimento_organizacional',2),(4,'Recrutamento e Seleção','recrutamento_e_selecao','<p>Desenvolvimento de políticas e práticas de recrutamento e seleção alinhados com a visão estratégica e de negócio da empresa. Contempla a implementação dos processos de recrutamento e seleção de estagiários e trainees.</p>','RH Estratégico','rh_estratégico',3),(5,'Avaliação de Potencial','avaliacao_de_potencial','<p>Análise aprofundada e sistematizada do perfil de um grupo de profissionais, pela aplicação de metodologia que visa avaliar as competências dos indivíduos através da observação direta e objetiva do comportamento. Áreas de aplicação: detecção do potencial, gestão de carreira, mobilidade interna, análise das necessidades de formação, processos de coaching.</p>','RH Estratégico','rh_estratégico',4),(6,'Remuneração Estratégica','remuneracao_estrategica','<p>Desenvolvimento e implantação de programas de remuneração integrados e alinhados com as estratégias organizacionais, gerando recompensas vinculadas aos resultados: individual, equipe e/ou organizacional, representados pelo grau de influência dos colaboradores no alcance dos objetivos.</p>','RH Estratégico','rh_estratégico',5),(7,'Coaching','coaching','<p>Acompanhamento de profissionais da empresa com o objetivo de atuar na expansão de sua capacidade de ação, contribuindo para ampliação do seu autoconhecimento, mobilizando seus recursos internos para a dissolução das barreiras que estejam restringindo suas possibilidades de aprendizagem e o pleno uso de seu potencial.</p>','Desenvolvimento Humano','desenvolvimento_humano',6),(8,'Programas de Desenvolvimento e Treinamento de Talentos','programas_de_desenvolvimento_e_treinamento_de_talentos','<p>Desenho de projetos específicos de desenvolvimento de talentos, desde cursos voltados para conhecimento técnico até workshops vivenciais para liderança, comunicação e equipe, entre outros.</p>','Desenvolvimento Humano','desenvolvimento_humano',7);
/*!40000 ALTER TABLE `fazemos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nucleos`
--

DROP TABLE IF EXISTS `nucleos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nucleos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `slug` varchar(140) DEFAULT NULL,
  `texto` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nucleos`
--

LOCK TABLES `nucleos` WRITE;
/*!40000 ALTER TABLE `nucleos` DISABLE KEYS */;
INSERT INTO `nucleos` VALUES (9,'NÚCLEO JOVEM (Authoria)','nucleo_jovem','<p><strong>Perfil</strong></p>\n<p>O <strong>N&uacute;cleo Jovem &ndash; Authoria</strong> &eacute; um programa voltado para pessoas de 18 a 28 anos&nbsp;que estejam passando por inquieta&ccedil;&otilde;es e questionamentos sobre a sua identidade&nbsp;profissional e que busquem ter papel mais ativo em suas vidas profissionais e na&nbsp;sociedade, desenvolvendo sua capacidade e habilidade para agir e realizar.</p>\n<p>O projeto foi criado com foco direcionado para a pr&aacute;tica que aborda o individuo&nbsp;integralmente. &Eacute; um projeto inovador baseado em 4 dimens&otilde;es:</p>\n<p><strong>&raquo;&nbsp;SER</strong></p>\n<p><strong>&raquo;&nbsp;CONVIVER</strong></p>\n<p><strong>&raquo;&nbsp;SABER</strong></p>\n<p><strong>&raquo; FAZER.</strong></p>\n<p>&nbsp;</p>\n<p><strong>Quais ferramentas utilizamos?</strong></p>\n<p>Nos encontros em grupos s&atilde;o utilizados jogos, din&acirc;micas e viv&ecirc;ncias para explorar a&nbsp;identidade e vis&atilde;o de mundo dos jovens. Para os encontros individuais s&atilde;o utilizados&nbsp;autobiografia, di&aacute;logo e atividades ligadas &agrave; explora&ccedil;&atilde;o da identidade profissional,&nbsp;abordando tanto o lado interno quanto externo de cada um.</p>'),(10,'NÚCLEO DE LIDERANÇA FEMININA','lideranca_feminina','<p><strong>Perfil</strong></p>\n<p>As mulheres v&ecirc;m conquistando cada vez mais espa&ccedil;o no mercado de trabalho e em&nbsp;cargos de lideran&ccedil;a, agregando, comprovadamente, benef&iacute;cios nas organiza&ccedil;&otilde;es&nbsp;como&nbsp;a&nbsp;melhoria&nbsp;na&nbsp;comunica&ccedil;&atilde;o&nbsp;entre&nbsp;pessoas,<br />maior&nbsp;consci&ecirc;ncia&nbsp;e&nbsp;responsabilidade social, melhor avalia&ccedil;&atilde;o nas tomadas de decis&atilde;o e no processo de&nbsp;gest&atilde;o.</p>\n<p>O programa &eacute; voltado para mulheres de 30 a 40 anos que busquem equilibrar os&nbsp;seus diferentes pap&eacute;is, abordando seus questionamentos na sociedade moderna.</p>\n<p>&nbsp;</p>\n<p><strong>Quais ferramentas utilizamos?</strong></p>\n<p>O trabalho do <strong>N&uacute;cleo Mulheres</strong> &eacute; divido em dois grupos: <strong>Ser Mulher na Empresa&nbsp;e Lideran&ccedil;a Feminina</strong>.</p>\n<p><strong>&ldquo;Ser Mulher na Empresa&rdquo;</strong> foi desenvolvido para avaliar seus desafios e pap&eacute;is&nbsp;desempenhados, propondo solu&ccedil;&otilde;es para aumento de performance e posicionando&nbsp;lideran&ccedil;as femininas de sucesso no mercado de trabalho.</p>\n<p><strong>&ldquo;Lideran&ccedil;a Feminina&rdquo;</strong> busca desenvolver a lideran&ccedil;a da mulher nas organiza&ccedil;&otilde;es,&nbsp;diagnosticando seus talentos (sabedoria + intelig&ecirc;ncia + criatividade) e identificando&nbsp;as ferramentas necess&aacute;rias para que ela alcance seu potencial, extraindo o melhor&nbsp;das caracter&iacute;sticas das mulheres de sucesso, como determina&ccedil;&atilde;o, motiva&ccedil;&atilde;o,&nbsp;criatividade, coragem e paci&ecirc;ncia / suporte afetivo e emocional positivo.</p>\n<p>&nbsp;</p>\n<p><strong>Pilares</strong></p>\n<p><strong>1. Eu comigo mesma:</strong> eu e meu corpo, contato com a ess&ecirc;ncia, minhas for&ccedil;as e fraquezas</p>\n<p><strong>2. Eu e o outro:</strong> como se caracterizam minhas rela&ccedil;&otilde;es com meu parceiro, chefe, subordinados, pais, filhos e amigos</p>\n<p><strong>3. Eu e o mundo:</strong> significado do trabalho e cidadania</p>\n<p><strong>4. Projeto de felicidade</strong></p>'),(11,'NÚCLEO DE FORMAÇÃO DE CONSULTORES GAIA EMPRESARIAL','formacao_de_consultores','<p><strong>Perfil</strong></p>\n<p>O programa de Consultores Gaia Empresarial foi desenvolvido com o objetivo de&nbsp;formar profissionais capacitados que possam fazer parte da rede de colaboradores&nbsp;da <strong>Gaia Capital Humano</strong>, atuando no processo de desenvolvimento do indiv&iacute;duo e&nbsp;das organiza&ccedil;&otilde;es.</p>\n<p>&nbsp;</p>\n<p><strong>Quais ferramentas utilizamos?</strong></p>\n<p>O programa &eacute; dividido em duas etapas. A primeira &eacute; voltada a compreender a&nbsp;cultura e valores da empresa, onde s&atilde;o analisadas quest&otilde;es como: a empresa&nbsp;(objetivo e raz&atilde;o de existir), a cultura organizacional, papel e ferramentas de apoio&nbsp;ao RH e desafio como parceiro estrat&eacute;gico.</p>\n<p>A segunda etapa aborda o desenvolvimento das habilidades em como trabalhar com grupos empresariais. Nesta fase est&atilde;o &nbsp;englobadas as vari&aacute;veis de autopercep&ccedil;&atilde;o,&nbsp;comunica&ccedil;&atilde;o, criatividade, lideran&ccedil;a e constru&ccedil;&atilde;o da identidade do indiv&iacute;duo.</p>'),(12,'NÚCLEO DE FORMAÇÃO DE COACH','formacao_de_coach','<p><strong>Perfil</strong></p>\n<p>O N&uacute;cleo de Forma&ccedil;&atilde;o de Coach &eacute; voltado para pessoas que buscam novos desafios,&nbsp;em momentos de transi&ccedil;&atilde;o de carreira ou processo seletivo para o primeiro&nbsp;emprego. Com uma <strong>metodologia desenvolvida exclusivamente pela Gaia&nbsp;Capital Humano</strong> o programa desenvolve ferramentas e solu&ccedil;&otilde;es para que o&nbsp;indiv&iacute;duo consiga tra&ccedil;ar sua trajet&oacute;ria e alcan&ccedil;&aacute;-la com sucesso.</p>\n<p>O Coaching &eacute; um convite &agrave; mudan&ccedil;a, a pensar diferente. &Eacute; se propor a estar em&nbsp;movimento e em busca do sentido mais amplo de ser. Trata-se de um processo de&nbsp;aprendizagem, no qual valores, cren&ccedil;as e desafios s&atilde;o trabalhados ao longo do&nbsp;processo.</p>\n<p>&nbsp;</p>\n<p><strong>Que ferramentas utilizamos?</strong></p>\n<p>O programa &eacute; desenvolvido baseado, em especial, em duas ferramentas a <strong>MBTI &ndash;&nbsp;Myers-Briggs Type Indicator</strong>, indicador de prefer&ecirc;ncias pessoais que diagnostica&nbsp;os tipos psicol&oacute;gicos com base nos estudos de C. G. Jung - propicia&nbsp;autoconhecimento e destaca pontos fortes e &aacute;reas de potencial para o&nbsp;desenvolvimento pessoal e profissional.</p>\n<p>A segunda &eacute; a FCP &ndash; Frame Coaching Process, onde atrav&eacute;s de ferramentas simples&nbsp;&eacute; poss&iacute;vel o indiv&iacute;duo buscar seu prop&oacute;sito. O trabalho &eacute; finalizado com plano de&nbsp;a&ccedil;&atilde;o para colocar em pr&aacute;tica o que a viv&ecirc;ncia identificou.</p>'),(13,'NÚCLEO EQUILÍBRIO (meditação, yoga, respiraçao)','equilibrio','<p><strong>Perfil</strong></p>\n<p>O <strong>N&uacute;cleo Equil&iacute;brio</strong> foi desenvolvido para trabalhar a integralidade do indiv&iacute;duo,&nbsp;em todos os aspectos que permeiam sua identidade profissional e pessoal, buscando&nbsp;o equil&iacute;brio entre os dois pap&eacute;is e propondo solu&ccedil;&otilde;es para alcan&ccedil;ar o pleno bem<br />estar f&iacute;sico, mental e emocional.</p>\n<p>&nbsp;</p>\n<p><strong>Quais ferramentas utilizamos?</strong></p>\n<p><strong>O trabalho pode ser desenvolvido em quatro processos:</strong> Yoga, Medita&ccedil;&atilde;o,&nbsp;Medita&ccedil;&atilde;o Ativa e Relaxamento, realizados de forma individual ou em grupo.</p>'),(14,'NÚCLEO TAYLOR MADE (Projetos Customizados)','taylor_made','<p>Baseado na ampla experi&ecirc;ncia na &aacute;rea de desenvolvimento humano e&nbsp;organizacional, oferecemos programas espec&iacute;ficos de acordo com a necessidade de&nbsp;cada empresa.</p>\n<p>Com atendimento exclusivo a Gaia Capital Humano se destaca em desenvolver&nbsp;solu&ccedil;&otilde;es &uacute;nicas e integradas, considerando as particularidades de cada projeto e&nbsp;com o compromisso de oferecer ferramentas criativas e ideias inovadoras.</p>');
/*!40000 ALTER TABLE `nucleos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(140) NOT NULL,
  `email` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `NomeUsuario` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (11,'trupe','d32c9694c72f1799ec545c82c8309c02','contato@trupe.net');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `imagem` varchar(140) DEFAULT NULL,
  `link` varchar(140) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capital`
--

DROP TABLE IF EXISTS `capital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capital` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `slug` varchar(140) DEFAULT NULL,
  `texto` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capital`
--

LOCK TABLES `capital` WRITE;
/*!40000 ALTER TABLE `capital` DISABLE KEYS */;
INSERT INTO `capital` VALUES (15,'Edmundo Barbosa','edmundo_barbosa','<p>Psicoterapeuta, com p&oacute;s-gradua&ccedil;&atilde;o pelo California Institute of Integral Studies, S&atilde;o&nbsp;Francisco, Calif&oacute;rnia (1978-1982), Edmundo Barbosa atuou como professor assistente na&nbsp;California State University, em S&atilde;o Francisco, de 1982 a 1983 e coordenou o programa no&nbsp;C&acirc;ncer Support and Education Center, em Palo Alto, de 1982 a 1985.</p>\n<p>Especializado em trabalho corporal neo-reichiano e estudioso da Ecopsicologia (o reconhecimento de que o equil&iacute;brio interno e externo do ser humano est&aacute; diretamente relacionado ao meio-ambiente no qual est&aacute; inserido), coordena projetos de forma&ccedil;&atilde;o de executores de a&ccedil;&otilde;es ligadas ao empreendedorismo social e preserva&ccedil;&atilde;o do meio ambiente.</p>\n<p>Ministra oficinas no Esalen Institute, California, al&eacute;m de coordenar cursos e treinamentos&nbsp;para profissionais na &aacute;rea da sa&uacute;de integrativa no Brasil, Europa e Estados Unidos. H&aacute; quase&nbsp;20 anos fundou o Instituto Gaia, onde dirige e desenvolve trabalhos voltados para a&nbsp;expans&atilde;o da consci&ecirc;ncia e do conhecimento humano nas &aacute;reas ligadas a psicologia, sa&uacute;de e&nbsp;autodesenvolvimento pessoal e profissional.</p>\n<p>&Eacute; tamb&eacute;m o fundador e diretor do Instituto ReVida que desde 1989 realiza programas de&nbsp;apoio a pacientes oncol&oacute;gicos e autor do livro &ldquo;Esta&ccedil;&otilde;es da Vida &ndash; Hist&oacute;rias de solidariedade&nbsp;e esperan&ccedil;a&rdquo;.</p>\n<p>Consultor de empresas desde 1998, desenvolve trabalhos e din&acirc;micas com grupos, e&nbsp;treinamentos, assim como coaching individual para clientes como: Hedging-Griffo, FSB&nbsp;Comunica&ccedil;&otilde;es, Schincariol, Mc Donalds, ABCP (Associa&ccedil;&atilde;o Brasileira de Cimento Portland),&nbsp;Bayer, Abbot, &Uacute;NICA (uni&atilde;o das ind&uacute;strias de a&ccedil;&uacute;car e &aacute;lcool). Atua como consultor&nbsp;associado, coordenador e supervisor do departamento de Coaches da Wisnet Consulting.</p>'),(16,'Renata Define','renata_define','<p>Bacharel em Administra&ccedil;&atilde;o de Empresas, pela FAAP e com MBA em Administra&ccedil;&atilde;o de&nbsp;Empresas, pelo Ibmec, atualmente, Renata Define, cursa o &uacute;ltimo ano da Faculdade de&nbsp;Psicologia, pela PUC-SP.</p>\n<p>S&oacute;cia-respons&aacute;vel pelas &aacute;reas Administrativo/ Financeiro e de Recursos Humanos na&nbsp;Hedging-Griffo Corretora de Valores S/A por oito anos, adquiriu ampla experi&ecirc;ncia em&nbsp;desenvolvimento de talentos, sendo respons&aacute;vel pela cria&ccedil;&atilde;o e &nbsp;implementa&ccedil;&atilde;o do Programa&nbsp;com foco em aprendizagem transformacional.</p>\n<p>Buscou conhecimentos na &aacute;rea de desenvolvimento humano passando por diversos cursos e&nbsp;viv&ecirc;ncias, como: Frameworks for Change &ndash; Coaching Process, Facilitadora da devolutiva do&nbsp;MBTI &ndash; IDH, Curso de Certifica&ccedil;&atilde;o Internacional de Coaching &ndash; Lambert do Brasil,&nbsp;Desenvolvendo seu l&iacute;der interior &ndash; Iniciativa Gaia, Jogo da transforma&ccedil;&atilde;o &ndash; Findhorn&nbsp;(Esc&oacute;cia), Desenvolvendo Rela&ccedil;&otilde;es Significativas &ndash; Escola de Marketing Industrial,&nbsp;Hierarquia sem Fronteiras &ndash; Pedro Mandelli entre outros.</p>'),(17,'Fábio Batista','fabio_batista','<p>Psic&oacute;logo graduado pela PUC-SP, possui especializa&ccedil;&atilde;o em Psicologia Social das&nbsp;Organiza&ccedil;&otilde;es pelo Instituto Sedes Sapientiae e p&oacute;s-gradua&ccedil;&atilde;o em Administra&ccedil;&atilde;o de&nbsp;Empresas pela FGV-SP.</p>\n<p>Como consultor atua nas &aacute;reas de Transi&ccedil;&atilde;o de Carreira, Gest&atilde;o do Ambiente Organizacional,&nbsp;Pesquisa de Clima, Avalia&ccedil;&atilde;o de perfis de lideran&ccedil;a, Coaching e Couseling, destacando-se os&nbsp;projetos desenvolvidos junto a empresas como C&amp;A, Banco Ita&uacute;-Unibanco, Basf, Telef&ocirc;nica/Vivo, entre outras. Iniciou sua carreira na &aacute;rea de Projetos e Processos em Sistemas de&nbsp;Informa&ccedil;&atilde;o, prestando consultoria a empresas como Ford, Unibanco AIG e Peugeot. Atuou&nbsp;na &aacute;rea de Relacionamento com Clientes Corporativos da Orbitall e ocupou cargo de&nbsp;lideran&ccedil;a na Alfa Seguros e Previd&ecirc;ncia at&eacute; redirecionar sua trajet&oacute;ria profissional para as&nbsp;&aacute;reas da Psicologia Cl&iacute;nica e Organizacional.</p>'),(18,'Beatriz Petrilli','beatriz_petrilli','<p>Graduada em Administra&ccedil;&atilde;o pela FAAP e p&oacute;s-graduada em Administra&ccedil;&atilde;o pela FGV-RJ,&nbsp;Beatriz Petrilli &eacute; psicodramatista pelo Instituto Sedes Sapientiae (SP) e est&aacute; em forma&ccedil;&atilde;o no&nbsp;Grupo Autodirigido do Instituto J.L. Moreno. Coach pela International Coaching Community&nbsp;atua em trabalhos de grupos focados em diagn&oacute;stico, transforma&ccedil;&atilde;o e consolida&ccedil;&atilde;o das&nbsp;rela&ccedil;&otilde;es em empresas, escolas, universidades e institui&ccedil;&otilde;es como aut&ocirc;noma e atrav&eacute;s de&nbsp;parcerias. Realiza consultoria para coaching individual e em grupo, &eacute; integrante do Grupo&nbsp;Improvise (teatro de Reprise), trabalha como coordenadora administrativa do Departamento&nbsp;de Psicodrama do Instituto Sedes Sapientiae e faz parte da equipe de &nbsp;consultores da Gaia&nbsp;Capital Humano.</p>');
/*!40000 ALTER TABLE `capital` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-04-18 13:15:17
