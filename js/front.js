function initPage() {
	var maps = $('map');
	if(maps) {
		maps.each( function(){
		    var links = $(this).find('area');
				
		    for(var i=0; i<links.length; i++){

			    $(links[i]).on('mouseover', function(e) {
			      	var link_id = this.id;
					link_id = link_id + '-img';
			      	$('#'+link_id).fadeIn('normal');
			    });
					
			    $(links[i]).on('mouseout', function(e) {
			    	var link_id = this.id;
					link_id = link_id + '-img';
					$('#'+link_id).fadeOut('normal');
			    });

			}
		});
	}
}

$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	if($('body').hasClass('ie-body-home')){
		initPage();
	}

	$('.box-wrapper .hexagon a').click( function(e){
		e.preventDefault();

		if (!$(this).hasClass('marcado')) {

			$('.box-wrapper .hexagon a.marcado').removeClass('marcado');
			$(this).addClass('marcado');

			var slug = $(this).attr('href').substring(20);
			var titulo = $(this).attr('title');
			var botao = "<a class='info-programa' href='mailto:contato@gaia.com.br?subject=Informações sobre o Programa "+titulo+"' title='Entre em Contato'><div class='texto'>Entre em contato conosco e conheça melhor o programa</div></a>";

			$.post(BASE+'ajax/pegarNucleo', { slug : slug }, function(resposta){
				$('.coluna2 #destino-texto').slideUp('fast', function(){
					$(this).html(resposta+botao).slideDown('fast', function(){
						$('html, body').animate({ 
							scrollTop : $('.coluna2 #destino-texto').offset().top
						}, 200);
					});
				});
			});
		};
	});

	$('#nav-conheca .hexagon a').click( function(e){
		e.preventDefault();

		if (!$(this).hasClass('marcado')) {

			$('#nav-conheca .hexagon a.marcado').removeClass('marcado');
			$(this).addClass('marcado');

			$('#div-conheca .texto').slideUp('fast', function(){
				$(this).html('abc').slideDown('fast', function(){
					$('html, body').animate({ 
						scrollTop : $('#div-conheca .texto').offset().top
					}, 200);
				});
			});

		};
	});

	$('.main-contato .form-container h1').click( function(){

		var contexto = $(this).parent('.main-contato .form-container');

		if (contexto.hasClass('hid')) {
			$('.main-contato .form-container').not(contexto).addClass('hid');
			contexto.removeClass('hid');
		};
	})

	$('#fale-conosco form').submit( function(){
		var contexto = $('#fale-conosco form');
		if ($('#input-nome', contexto).val() == "" || $('#input-nome', contexto).val() == $('#input-nome', contexto).attr('placeholder')) {
			alert("Informe seu nome!");
			return false;
		};
		if ($('#input-email', contexto).val() == "" || $('#input-email', contexto).val() == $('#input-email', contexto).attr('placeholder')) {
			alert("Informe seu e-mail!");
			return false;
		};
		if ($('#input-mensagem', contexto).val() == "" || $('#input-mensagem', contexto).val() == $('#input-mensagem', contexto).attr('placeholder')) {
			alert("Informe sua mensagem!");
			return false;
		};
	});

	$('#trabalhe-conosco form').submit( function(){
		var contexto = $('#trabalhe-conosco form');
		if ($('#input-nome', contexto).val() == "" || $('#input-nome', contexto).val() == $('#input-nome', contexto).attr('placeholder')) {
			alert("Informe seu nome!");
			return false;
		};
		if ($('#input-email', contexto).val() == "" || $('#input-email', contexto).val() == $('#input-email', contexto).attr('placeholder')) {
			alert("Informe seu e-mail!");
			return false;
		};
		if ($('#input-telefone', contexto).val() == "" || $('#input-telefone', contexto).val() == $('#input-telefone', contexto).attr('placeholder')) {
			alert("Informe um telefone de contato!");
			return false;
		};
		if ($('#input-area_interesse', contexto).val() == "" || $('#input-area_interesse', contexto).val() == $('#input-area_interesse', contexto).attr('placeholder')) {
			alert("Informe sua área de interesse!");
			return false;
		};
		if($('#input-cv', contexto).val() == ''){
			alert("Anexe seu CV!");
			return false;	
		}
	});

});