<?php

$lang['imglib_source_image_required'] = 'Voc� deve especificar uma imagem de origem em suas prefer�ncias.';
$lang['imglib_gd_required'] = 'A biblioteca de imagens GD � requerida para esta funcionalidade.';
$lang['imglib_gd_required_for_props'] = 'Seu servidor deve suportar a biblioteca de imagens GD para determinar as propriedades dessa imagem.';
$lang['imglib_unsupported_imagecreate'] = 'Seu servidor n�o suporta a fun��o GD requerida para processar este tipo de imagem.';
$lang['imglib_gif_not_supported'] = 'Imagens GIF por vezes n�o s�o suportadas devido a restri��es de licen�a. Voc� pode usar JPG ou PNG em seu lugar.';
$lang['imglib_jpg_not_supported'] = 'Imagens JPG n�o s�o suportadas.';
$lang['imglib_png_not_supported'] = 'Imagens PNG n�o s�o suportadas.';
$lang['imglib_jpg_or_png_required'] = 'O protocolo de redimensionamento de imagens espeficicada em suas prefer�ncias funciona somente com imagens do tipo JPEG ou PNG.';
$lang['imglib_copy_error'] = 'Um erro foi encontrado enquanto tentava substituir o arquivo. Confira se o diretório do arquivo � grav�vel.';
$lang['imglib_rotate_unsupported'] = 'Rota��o de imagens n�o parece ser suportada pelo seu servidor.';
$lang['imglib_libpath_invalid'] = 'O caminho para a biblioteca de imagens n�o est� correto. Por favor, defina o caminho correto em suas prefer�ncias de imagens.';
$lang['imglib_image_process_failed'] = 'Processamento da imagem falhou. Por favor, verifique se o seu servidor suporta o protocolo escolhido e o caminho para a sua biblioteca de imagens est� correto.';
$lang['imglib_rotation_angle_required'] = 'Um �ngulo de rota��o � necess�rio para rotacionar a imagem.';
$lang['imglib_writing_failed_gif'] = 'Imagem GIF.';
$lang['imglib_invalid_path'] = 'O caminho para a imagem n�o � v�lido.';
$lang['imglib_copy_failed'] = 'A rotina de copia da imagem falhou.';
$lang['imglib_missing_font'] = 'N�o foi poss�vel encontrar uma fonta para ser usada.';
$lang['imglib_save_failed'] = 'N�o foi poss�vel salvar a imagem. Por favor, verifique se a imagem e o diret�rio do arquivo � grav�vel.';
?>