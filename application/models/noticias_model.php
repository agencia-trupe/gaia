<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'noticias';
        $this->tabela_comentarios = 'noticias_comentarios';

        $this->dados = array('titulo', 'slug', 'data', 'imagem', 'olho', 'texto');
        $this->dados_tratados = array(
        	'slug' => url_title($this->input->post('titulo'), '_', TRUE),
        	'data' => formataData($this->input->post('data'), 'br2mysql'),
        	'imagem' => $this->sobeImagem()
        );
	}

/*
CREATE TABLE `noticias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(140) DEFAULT NULL,
  `slug` varchar(140) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `autor` varchar(140) DEFAULT NULL,
  `olho` text,
  `texto` text,
  `imagem` varchar(140) DEFAULT NULL,
  `data_postagem` datetime DEFAULT NULL,
  `usuario_postagem` varchar(140) DEFAULT NULL,
  `data_ultima_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1

Tabela para comentários de autores cadastrados

CREATE TABLE `noticias_comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` int(11) DEFAULT NULL,
  `comentario` text,
  `data` datetime DEFAULT NULL,
  `id_noticia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

Tabela para comentários de quaisquer autores

CREATE TABLE `noticias_comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autor` varchar(140) DEFAULT NULL,
  `comentario` text,
  `data` datetime DEFAULT NULL,
  `id_noticia` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
*/

	function pegarComentarios($id, $retornaContagem = FALSE){
		if ($retornaContagem) {
			return $this->db->get_where($this->tabela_comentarios, array('id_noticia' => $id))->num_rows();
		} else {
			$comentarios = $this->db->order_by('data', 'DESC')->get_where($this->tabela_comentarios, array('id_noticia' => $id))->result();
			foreach ($comentarios as $key => $value) {
				$value->autor = $this->pegarAutorComentario($value->autor);
			}
			return $comentarios;
		}
	}

	function pegarComentariosPaginados($id_noticia, $por_pagina, $inicio){
		return $this->db->order_by('data', 'desc')->get_where($this->tabela_comentarios, array('id_noticia' => $id_noticia), $por_pagina, $inicio)->result();
	}

	function pegarAutorComentario($id){
		$qry = $this->db->get_where("cadastros_comentarios", array('id' => $id))->result();
		if(isset($qry[0]) && $qry[0])
			return $qry[0];
		else
			return false;
	}

	function excluirComentario($id){
		return $this->db->where('id', $id)->delete($this->tabela_comentarios);
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/noticias/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'max_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo
	        		->load($original['dir'].$filename)
	                ->resize(598,1000)
	                ->resize_crop(598, 398)
	                ->save($original['dir'].$filename, TRUE)
	                ->resize_crop(100, 100)
	                ->save($original['dir'].'thumbs/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}		
	}	

}
