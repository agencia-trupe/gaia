<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = "cadastros";

        $this->dados = array();
        $this->dados_tratados = array();
	}

/*
CREATE TABLE `cadastros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `data_cadastro` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1$$
*/
}
