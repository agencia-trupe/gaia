<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nucleos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'nucleos';

		$this->dados = array('texto');
		$this->dados_tratados = array();
	}

	function pegarTodos($order_campo = 'id', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela)->result();
	}
	
	function pegarPaginado($por_pagina, $inicio, $order_campo = 'id', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}
}