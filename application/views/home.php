<div class="caixa-superior">

	<div class="tagline">Desenvolver e transformar</div>
	<div class="tagline2">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;através de <strong>experiências legítimas</strong><br>
      	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;que despertem o <strong>potencial</strong><br>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;do <strong>indivíduo</strong> e da <strong>organização</strong>
	</div>

</div>

<div class="hive">

	<div class="celula" id="celula-desenvolvimento">

		<h1>Desenvolvimento Organizacional</h1>
	
		<div class="hexagon hexagon2 hex-container-absolute" id="primeiro-home">
			<div class="hexagon-in1">
				<a href="o-que-fazemos" class="hexagon-in2 marinho">
					Diagnóstico<br>Organizacional
				</a>
			</div>
		</div>
		
		<div class="hexagon hexagon2 hex-container-absolute" id="segundo-home">
			<div class="hexagon-in1">
				<a href="o-que-fazemos" class="hexagon-in2 medio">
					Pesquisa de<br>Clima e Cultura
				</a>
			</div>
		</div>

		<div class="hexagon hexagon2 hex-container-absolute" id="terceiro-home">
			<div class="hexagon-in1">
				<div class="hexagon-in2" id="imagem-post-it">							
				</div>
			</div>
		</div>
	
		<div class="hexagon hexagon2 hex-container-absolute" id="quarto-home">
			<div class="hexagon-in1">
				<a href="o-que-fazemos" class="hexagon-in2 claro">
					Gestão por<br>Competência
				</a>
			</div>
		</div>

		<div class="hexagon hexagon2 hex-container-absolute" id="quinto-home">
			<div class="hexagon-in1">
				<div class="hexagon-in2" id="imagem-chuva">							
				</div>
			</div>
		</div>

		<div id="container-mapa1" class="versao-mapa">
			<img src="_imgs/layout/transp.gif" class="trasnp" usemap="#mapa1">
			<img src="_imgs/layout/mapa-home-1.png" alt="Desenvolvimento Organizacional" class="imgmapa">
			<map name="mapa1">
				<area id="mapa1-link1" alt="Diagnóstico Organizacional" title="Diagnóstico Organizacional" href="o-que-fazemos" shape="poly" coords="190,3,282,59,282,162,190,215,98,162,98,55"></area>
				<area id="mapa1-link2" alt="Pesquisa de Clima e Cultura" title="Pesquisa de Clima e Cultura" href="o-que-fazemos" shape="poly" coords="96,172,184,224,185,325,95,376,7,325,8,224"></area>
				<area id="mapa1-link3" alt="Gestão por Competência" title="Gestão por Competência" href="o-que-fazemos" shape="poly" coords="190,338,279,391,279,491,191,544,98,490,100,390"></area>
			</map>
			<ul>
				<li id="mapa1-link1-img"><a href="o-que-fazemos" title="Diagnóstico Organizacional">Diagnóstico<br>Organizacional</a></li>
				<li id="mapa1-link2-img"><a href="o-que-fazemos" title="Pesquisa de Clima e Cultura">Pesquisa de<br>Clima e Cultura</a></li>
				<li id="mapa1-link3-img"><a href="o-que-fazemos" title="Gestão por Competência">Gestão por<br>Competência</a></li>
			</ul>
		</div>
		
	</div>

	<div class="celula" id="celula-rh">

		<h1>RH Estratégico</h1>
	
		<div class="hexagon hexagon2 hex-container-absolute" id="primeiro-home">
			<div class="hexagon-in1">
				<a href="o-que-fazemos" class="hexagon-in2 marinho">
					Recrutamento<br>e Seleção
				</a>
			</div>
		</div>
		
		<div class="hexagon hexagon2 hex-container-absolute" id="segundo-home">
			<div class="hexagon-in1">
				<a href="o-que-fazemos" class="hexagon-in2 medio">
					Avaliação<br>de Potencial
				</a>
			</div>
		</div>

		<div class="hexagon hexagon2 hex-container-absolute" id="terceiro-home">
			<div class="hexagon-in1">
				<div class="hexagon-in2" id="imagem-maos">							
				</div>
			</div>
		</div>
	
		<div class="hexagon hexagon2 hex-container-absolute" id="quarto-home">
			<div class="hexagon-in1">
				<a href="o-que-fazemos" class="hexagon-in2 claro">
					Remuneração<br>Estratégica
				</a>
			</div>
		</div>

		<div class="hexagon hexagon2 hex-container-absolute" id="quinto-home">
			<div class="hexagon-in1">
				<div class="hexagon-in2" id="imagem-pensando">							
				</div>
			</div>
		</div>		
		
		<div id="container-mapa2" class="versao-mapa">
			<img src="_imgs/layout/transp.gif" class="trasnp" usemap="#mapa2">
			<img src="_imgs/layout/mapa-home-2.png" alt="RH Estratégico" class="imgmapa">
			<map name="mapa2">
				<area id="mapa2-link1" alt="Recrutamento e Seleção" title="Recrutamento e Seleção" href="o-que-fazemos" shape="poly" coords="190,3,282,59,282,162,190,215,98,162,98,55"></area>
				<area id="mapa2-link2" alt="Avaliação de Potencial" title="Avaliação de Potencial" href="o-que-fazemos" shape="poly" coords="96,172,184,224,185,325,95,376,7,325,8,224"></area>
				<area id="mapa2-link3" alt="Remuneração Estratégica" title="Remuneração Estratégica" href="o-que-fazemos" shape="poly" coords="190,338,279,391,279,491,191,544,98,490,100,390"></area>
			</map>
			<ul>
				<li id="mapa2-link1-img"><a href="o-que-fazemos" title="Recrutamento e Seleção">Recrutamento<br>e Seleção</a></li>
				<li id="mapa2-link2-img"><a href="o-que-fazemos" title="Avaliação de Potencial">Avaliação<br>de Potencial</a></li>
				<li id="mapa2-link3-img"><a href="o-que-fazemos" title="Remuneração Estratégica">Remuneração<br>Estratégica</a></li>
			</ul>
		</div>

	</div>

	<div class="celula" id="celula-humano">

		<h1 style="text-align:right;">Desenvolvimento<br> Humano</h1>
	
		<div class="hexagon hexagon2 hex-container-absolute" id="primeiro-home">
			<div class="hexagon-in1">
				<a class="hexagon-in2 marinho">
					Coaching
				</a>
			</div>
		</div>
		
		<div class="hexagon hexagon2 hex-container-absolute" id="segundo-home">
			<div class="hexagon-in1">
				<a class="hexagon-in2 medio hex-semi-padding">
					Programas de<br>Desenvolvimento<br>e Treinamento<br>de Talentos
				</a>
			</div>
		</div>

		<div class="hexagon hexagon2 hex-container-absolute" id="terceiro-home">
			<div class="hexagon-in1">
				<div class="hexagon-in2" id="imagem-bote">							
				</div>
			</div>
		</div>

		<div class="hexagon hexagon2 hex-container-absolute" id="quarto-home">
			<div class="hexagon-in1">
				<div class="hexagon-in2" id="imagem-vazio">							
				</div>
			</div>
		</div>

		<div id="container-mapa3" class="versao-mapa">
			<img src="_imgs/layout/transp.gif" class="trasnp" usemap="#mapa3">
			<img src="_imgs/layout/mapa-home-3.png" alt="Desenvolvimento Humano" class="imgmapa">
			<map name="mapa3">
				<area id="mapa3-link1" alt="" title="" href="o-que-fazemos" shape="poly" coords="95,213,7,162,7,56,97,6,185,59,186,161"></area>
				<area id="mapa3-link2" alt="" title="" href="o-que-fazemos" shape="poly" coords="194,161,197,59,286,5,375,59,376,160,285,214"></area>
			</map>
			<ul>
				<li id="mapa3-link1-img"><a href="o-que-fazemos" title="Coaching">Coaching</a></li>
				<li id="mapa3-link2-img"><a href="o-que-fazemos" title="Programas de Desenvolvimento e Treinamento de Talentos">Programas de<br>Desenvolvimento<br>e Treinamento<br>de Talentos</a></li>
			</ul>
		</div>

	</div>

</div>