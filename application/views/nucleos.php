<div class="box-wrapper">

	<div class='hexagon' id="link1"><a class="treslinhas" href="nucleos-de-trabalho/<?=$registro[0]->slug?>" title="<?=$registro[0]->titulo?>">NÚCLEO<br>JOVEM<br>(Authoria)</a></div>
	<div class='hexagon' id="link2"><a class="treslinhas" href="nucleos-de-trabalho/<?=$registro[1]->slug?>" title="<?=$registro[1]->titulo?>">NÚCLEO<br>DE LIDERANÇA<br>FEMININA</a></div>

	<div class='hexagon' id="link3"><a class="cincolinhas" href="nucleos-de-trabalho/<?=$registro[2]->slug?>" title="<?=$registro[2]->titulo?>">NÚCLEO DE<br>FORMAÇÃO DE<br>CONSULTORES<br>GAIA<br>EMPRESARIAL</a></div>
	<div class='hexagon' id="link4"><a class="treslinhas" href="nucleos-de-trabalho/<?=$registro[3]->slug?>" title="<?=$registro[3]->titulo?>">NÚCLEO DE<br>FORMAÇÃO<br>DE COACH</a></div>

	<div class='hexagon' id="link5"><a class="quatrolinhas" href="nucleos-de-trabalho/<?=$registro[4]->slug?>" title="<?=$registro[4]->titulo?>">NÚCLEO<br>EQUILÍBRIO<br>(meditação, yoga,<br>respiração)</a></div>
	<div class='hexagon' id="link6"><a class="quatrolinhas" href="nucleos-de-trabalho/<?=$registro[5]->slug?>" title="<?=$registro[5]->titulo?>">NÚCLEO<br>TAYLOR MADE<br>(Projetos<br>Customizados)</a></div>

</div>

<div class="coluna2">

	<h1>NÚCLEOS<br>de trabalho</h1>

	<div id="destino-texto">

		<p>
			Além dos processos e atividades relacionados ao <strong>desenvolvimento organizacional</strong> e voltados à transformação do indivíduo no ambiente de trabalho, a <strong>Gaia Capital Humano</strong> especializou-se em alguns grupos de trabalhos (Núcleos), de forma a oferecer soluções ainda mais personalizadas de acordo com o público de cada processo. 
		</p>

		<p>
			<br>
			<br>
			<br>
			<strong>« CLIQUE NOS NÚCLEOS PARA SABER MAIS</strong>
		</p>

	</div>

</div>