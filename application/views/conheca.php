<div class="caixa-superior">

	<h1>Conheça a<br>GAIA Capital Humano</h1>

	<p>
		A <strong>Gaia Capital Humano</strong> nasceu da iniciativa de desenvolver e transformar, através de experiências legítimas que despertem o potencial do indivíduo e da organização. 
	</p>
	<p>
		Em nossos projetos desenvolvemos <strong>soluções personalizadas</strong> com propostas que promovam o desenvolvimento humano dentro das organizações, focadas em explorar o autoconhecimento e ajudando cada indivíduo a <strong>alcançar seu potencial</strong>. 
	</p>
	<p>
		Atuamos na construção de políticas, práticas e processos organizacionais com a finalidade de <strong>agregar, recompensar, motivar e desenvolver pessoas</strong>. O objetivo é ser um facilitador para que cada um encontre seu papel dentro das organizações, evidenciando suas competências e recursos necessários ao processo de mudança. 
	</p>

</div>

<h2>Nossa abordagem</h2>

<div class="grafico">
	<p class="hexagono laranja">
		O trabalho se inicia a partir da compreensão da cultura organizacional, de suas diretrizes estratégicas e do setor de atuação da empresa, bem como dos objetivos a serem alcançados durante o processo.
	</p>

	<p class="hexagono laranja meio">
		Os projetos desenvolvidos são realizados através de workshops, dinâmicas e vivências, levando os indivíduos a se sensibilizarem através da experimentação. Desta forma, a organização internaliza os novos referenciais de maneira profunda e eles se sustentam no tempo.
	</p>

	<p class="hexagono laranja">
 		Esta abordagem garante uma construção autêntica e profunda de novos parâmetros para todo o grupo, propiciando mudanças efetivas e trazendo resultados concretos para a organização no curto, médio e longo prazo.
	</p>

	<div class="valores">

		<h3>Nossos<br>VALORES</h3>

		<p class="hexagono azul azul5 topo">
			<strong>Desenvolvimento</strong><br>
			Acreditamos na capacidade de desenvolvimento do ser humano, desconstruímos para mudar a forma e ampliar visão e forma de estar no mundo
		</p>

		<p class="hexagono azul azul4 meio topo">
			<strong>Potência</strong><br>			
			Trabalhamos objetivando resultado e realização de potência, com o intuito de concretizar nossos sonhos e aspirações 
		</p>

		<p class="hexagono azul azul3 topo">
			<strong>Integralidade</strong><br>
			Desenvolvemos um trabalho fundado na integralidade do ser humano, considerando o lado mental e emocional, e toda complexidade envolvida
		</p>

		<p class="hexagono azul azul2 meio-espaco-esquerda">
			<strong>Concretude</strong><br>			
			Apoiamos nossos clientes criando possibilidade de concretizar suas aspirações em relação a sua identidade profissional
		</p>

		<p class="hexagono azul azul1">
			<strong>Singularidade</strong><br>
			Consideramos em nosso trabalho a forma de ser e estar no mundo de cada um, isso faz com que cada cliente seja atendido de forma especial e singular
		</p>

	</div>
</div>

<h4>
	Quem ATENDEMOS?
	<p>
		Organizações de pequeno e médio porte que estejam em fase de expansão e crescimento e que busquem repensar e reorganizar sua estratégia ligada à prática de gestão de pessoas. 
	</p>
</h4>