<div class="caixa-superior">
	<h1>Alguns de nossos<br>
	CLIENTES</h1>
</div>

<span>
	<?php if ($clientes): ?>
		<?php foreach ($clientes as $key => $value): ?>
			<a href="<?=$value->link?>" title="<?=$value->titulo?>" class="link-clientes" target="_blank"><img src="_imgs/clientes/<?=$value->imagem?>" alt="<?=$value->titulo?>"></a>
		<?php endforeach ?>
	<?php else: ?>

		Nenhum Cliente

	<?php endif ?>
</span>