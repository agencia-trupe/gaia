<div class="caixa-superior">
	<h1>Nosso<br>
	CAPITAL HUMANO</h1>

	<p>
		A <strong>Gaia Capital Humano</strong> trabalha com um grupo de profissionais de repertório amplo, que propicia o alcance de soluções originais, criativas e alinhadas às necessidades dos diferentes negócios. 
	</p>
	<p>
		Com uma visão holística e vasta experiência, apoiamos indivíduos, grupos e organizações para que se tornem mais conscientes e eficientes, atingindo resultados almejados e realizando-se plenamente na vida profissional e pessoal, promovendo a evolução nos ambientes corporativos.
	</p>
	
	<h2>Equipe</h2>
	<p class="sem-margem">
		Dirigida pelos sócios <strong>Edmundo Barbosa</strong> e <strong>Renata Define</strong> a Gaia Capital Humano conta com uma rede de parceiros e colaboradores na operacionalização do negócio e implementação dos processos. 
	</p>
	<p>
		Psicoterapeuta, com pós-graduação pelo California Institute of Integral Studies, Edmundo Barbosa é responsável pelas áreas de desenvolvimento, dinâmicas de grupo, temáticas de desenvolvimento, relacionamento e contatos internacionais. 
	</p>
	<p>
		Renata Define, bacharel em Administração de Empresas, pela FAAP e com MBA em Administração de Empresas, pelo Ibmec, coordena as áreas voltadas a projetos corporativos, recursos humanos, cultura organizacional e relacionamento. 
	</p>

</div>

<h3>Conheça-nos</h3>

<div id="nav-conheca">

	<div class='hexagon' id="link1"><a class="marcado" href="nosso-capital-humano/edmundo" data-target="edmundo" title="Edmundo Barbosa">Edmundo<br>Barbosa</a></div>
	<div class='hexagon' id="link2"><a href="nosso-capital-humano/renata" data-target="renata" title="Renata Define">Renata<br>Define</a></div>

	<div class='hexagon' id="link3"><a href="nosso-capital-humano/fabio" data-target="fabio" title="Fábio Batista">Fábio<br>Batista</a></div>
	<div class='hexagon' id="link4"><a href="nosso-capital-humano/beatriz" data-target="beatriz" title="Beatriz Petrilli">Beatriz<br>Petrilli</a></div>

</div>

<div id="div-conheca">

	<div class="texto">
		<h2>Edmundo barbosa</h2>

		<p>
			Psicoterapeuta, com pós-graduação pelo California Institute of Integral Studies, São Francisco, Califórnia (1978-1982), Edmundo Barbosa atuou como professor assistente na California State University, em São Francisco, de 1982 a 1983 e coordenou o programa no Câncer Support and Education Center, em Palo Alto, de 1982 a 1985. 
		</p>
		<p>
			Especializado em trabalho corporal neo-reichiano e estudioso da Ecopsicologia (o reconhecimento de que o equilíbrio interno e externo do ser humano está diretamente relacionado ao meio-ambiente no qual está inserido), coordena projetos de formação de executores de ações ligadas ao empreendedorismo social e preservação do meio ambiente. 
		</p>
		<p>
			Ministra oficinas no Esalen Institute, California, além de coordenar cursos e treinamentos para profissionais na área da saúde integrativa no Brasil, Europa e Estados Unidos. Há quase 20 anos fundou o Instituto Gaia, onde dirige e desenvolve trabalhos voltados para a expansão da consciência e do conhecimento humano nas áreas ligadas a psicologia, saúde e autodesenvolvimento pessoal e profissional. 
		</p>
		<p>
			É também o fundador e diretor do Instituto ReVida que desde 1989 realiza programas de apoio a pacientes oncológicos e autor do livro "Estações da Vida – Histórias de solidariedade e esperança". 
		</p>
		<p>
			Consultor de empresas desde 1998, desenvolve trabalhos e dinâmicas com grupos, e treinamentos, assim como coaching individual para clientes como: Hedging-Griffo, FSB Comunicações, Schincariol, Mc Donalds, ABCP (Associação Brasileira de Cimento Portland), Bayer, Abbot, ÚNICA (união das indústrias de açúcar e álcool). Atua como consultor associado, coordenador e supervisor do departamento de Coaches da Wisnet Consulting. 
		</p>
	</div>

</div>