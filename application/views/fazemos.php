<div class="caixa-superior">
	<h1>O que<br>FAZEMOS?</h1>
</div>

<h2 class="azul5 r">Desenvolvimento Organizacional</h2>

<div class="linha r azul5">
	<div class="hexagono">
		Diagnóstico<br>Organizacional
	</div>
	<?=$registro[0]->texto?>
</div>

<div class="linha l azul5">
	<div class="hexagono">
		Pesquisa de<br>Clima e Cultura
	</div>
	<?=$registro[1]->texto?>
</div>

<div class="linha r azul5 ultima">
	<div class="hexagono">
		Gestão por<br>Competência
	</div>
	<?=$registro[2]->texto?>
</div>

<h2 class="azul_medio l">RH Estratégico</h2>

<div class="linha l azul_medio">
	<div class="hexagono">
		Recrutamento<br>e Seleção
	</div>
	<?=$registro[3]->texto?>
</div>

<div class="linha r azul_medio">
	<div class="hexagono">
		Avaliação<br>de Potencial
	</div>
	<?=$registro[4]->texto?>
</div>

<div class="linha l azul_medio ultima">
	<div class="hexagono">
		Remuneração<br>Estratégica
	</div>
	<?=$registro[5]->texto?>
</div>

<h2 class="azul_claro r">Desenvolvimento Humano</h2>

<div class="linha r azul_claro">
	<div class="hexagono">
		Coaching
	</div>
	<?=$registro[6]->texto?>
</div>

<div class="linha l azul_claro ultima">
	<div class="hexagono quatro-linhas">
		Programas de<br>Desenvolvimento<br>e Treinamento<br>de Talentos
	</div>
	<?=$registro[7]->texto?>
</div>