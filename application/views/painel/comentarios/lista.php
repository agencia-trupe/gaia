<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

  <div class="row">
    <div class="span12 columns">

      <?php if ($area != 'mapa'): ?>
        <a href="painel/<?=$area?>/<?=$lang?>" class="btn">voltar</a><br><br>  
      <?php endif ?>

      <?php if ($comentarios): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable">

          <thead>
            <tr>
              <!-- <th>Ordenar</th> -->
              <th class="yellow header headerSortDown">Autor</th>
              <th class="header">Data</th>
              <th class="header">Texto</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($comentarios as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <!-- <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td> -->
                  <td class="nowrap"><?=$value->autor->nome_assinatura?></td>
                  <td><?=formataData($value->data, 'mysql2br')?></td>
                  <td><?=$value->comentario?></td>
                  <td class="crud-actions" style="width:60px;">
                    <a href="painel/<?=$this->router->class?>/excluir/<?=$lang?>/<?=$area?>/<?=$value->id?>/<?=$id_noti?>" class="btn btn-danger btn-delete">excluir</a>
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

      <?php else:?>

        <h2>Nenhum Comentário Cadastrado</h2>

      <?php endif ?>



    </div>
  </div>