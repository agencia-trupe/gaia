<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="painel/noticias/index/">Notícias</a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="painel/noticias/form/"><?=$titulo?></a>
    	</li>
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		Imagem
		<?php if ($registro->imagem): ?>
			<br><img src="_imgs/clientes/<?=$registro->imagem?>"><br>			
		<?php endif ?>
		<label><input type="file" name="userfile"></label>

		<label>Endereço do Link
		<input type="text" name="link" required value="<?=$registro->link?>"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir/')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Título
		<input type="text" name="titulo" required autofocus></label>

		<label>Imagem
		<input type="file" name="userfile"></label>		

		<label>Endereço do Link
		<input type="text" name="link" required autofocus></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>