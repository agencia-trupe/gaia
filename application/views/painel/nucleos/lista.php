<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed">

          <thead>
            <tr>
              <th class="yellow header headerSortDown">Título</th>
              <th class="header">Texto</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>
              
                <tr class="tr-row" id="row_<?=$value->id?>">
                  <td class="nowrap"><?=$value->titulo?></td>
                  <td><?=word_limiter(strip_tags($value->texto), 15)?></td>
                  <td class="crud-actions" style="width:53px;">
                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if ($paginacao): ?>          
          <div class="pagination">
            <ul>        
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

        <h2>Nenhum Texto Cadastrado</h2>

      <?php endif ?>



    </div>
  </div>