<div class="container top">

	<ul class="breadcrumb">
    	<li>
      		<a href="painel/adesoes/index/<?=$lang?>">Adesões </a> <span class="divider">/</span>
    	</li>
    	<li class="active">
      		<a href="painel/adesoes/form/<?=$lang?>"><?=$titulo_bread?></a>
    	</li>
  </ul>

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<div id="dialog"></div>

	<?php if ($registro->tipo == 1): ?>

		<h3>Tipo: Pessoa Física</h3>

		<h3>Nome: <?=$registro->nome?></h3>

		<h3>País: <?=$registro->pais?></h3>

		<h3>E-mail: <?=$registro->email?></h3>

		<h3>Data de adesão: <?=formataData($registro->data, 'mysql2br')?></h3>

	<?php else: ?>

		<h3>Tipo: Empresa</h3>

		<h3>Empresa: <?=$registro->empresa?></h3>

		<h3>Contato: <?=$registro->contato_responsavel?></h3>

		<h3>Email: <?=$registro->email?></h3>

		<h3>País: <?=$registro->pais?></h3>

		<h3>Website: <?=$registro->website?></h3>

		<h3>Data de adesão: <?=formataData($registro->data, 'mysql2br')?></h3>

		<h3>Marca: <br> <img src="_imgs/apoio/<?=$registro->imagem?>"></h3>

	<?php endif ?>

	<div class="form-actions">
    	<a href="painel/<?=$this->router->class?>/excluir/<?=$registro->tipo?>/<?=$registro->id?>" class="btn btn-danger btn-delete">excluir</a>
    	<button class="btn btn-voltar" type="reset">Voltar</button>
  	</div>	
	
<?endif ?>