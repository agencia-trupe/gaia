<div class="navbar navbar-fixed-top">

  <div class="navbar-inner">

    <div class="container">

      <a href="painel/home" class="brand">Gaia</a>

      <ul class="nav">

        <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

        <li <?if($this->router->class=='fazemos')echo" class='active'"?>><a href="painel/fazemos">O que Fazemos?</a></li>

        <li <?if($this->router->class=='nucleos')echo" class='active'"?>><a href="painel/nucleos">Núcleos de Trabalho</a></li>

        <li <?if($this->router->class=='capital')echo" class='active'"?>><a href="painel/capital">Nosso Capital Humano</a></li>

        <li <?if($this->router->class=='clientes')echo" class='active'"?>><a href="painel/clientes">Clientes</a></li>

        <li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="painel/usuarios">Usuários</a></li>
            <li><a href="painel/home/logout">Logout</a></li>
          </ul>
        </li>

      </ul>

    </div>
  </div>
</div>