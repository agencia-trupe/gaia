<div class="caixa-superior">

	<h1 class="com-margem">Fale direto <span class="telefone">11 5581.6766</span></h1>

	<h1>Onde estamos</h1>

	<div class="container-mapa">

		<div class="coluna com-margem">
			<p>				
				Rua Maysa Figueira Monjardim, 67 <br>
				Vila Clementino • São Paulo, SP<br>
				04042-050
			</p>
			<iframe width="250" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?q=Rua+Maysa+Figueira+Monjardim,+67&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Maysa+Figueira+Monjardim,+67+-+Sa%C3%BAde,+S%C3%A3o+Paulo,+04042-050&amp;gl=br&amp;ll=-23.603254,-46.647709&amp;spn=0.027056,0.049524&amp;t=m&amp;z=14&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com.br/maps?q=Rua+Maysa+Figueira+Monjardim,+67&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Maysa+Figueira+Monjardim,+67+-+Sa%C3%BAde,+S%C3%A3o+Paulo,+04042-050&amp;gl=br&amp;ll=-23.603254,-46.647709&amp;spn=0.027056,0.049524&amp;t=m&amp;z=14&amp;source=embed">Exibir mapa ampliado</a></small>
		</div>

		<div class="coluna">
			<p>
				Rua Iguatemi, 284 [vila] casa 09<br>
				Itaim Bibi • São Paulo, SP<br>
				01010-001
			</p>
			<iframe width="250" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com.br/maps?f=q&amp;source=s_q&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Iguatemi,+284&amp;aq=&amp;sll=-23.602453,-46.645632&amp;sspn=0.027056,0.049524&amp;gl=br&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Iguatemi,+284+-+Itaim+Bibi,+S%C3%A3o+Paulo,+01451-010&amp;t=m&amp;z=14&amp;ll=-23.583677,-46.682767&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com.br/maps?f=q&amp;source=embed&amp;hl=pt-BR&amp;geocode=&amp;q=Rua+Iguatemi,+284&amp;aq=&amp;sll=-23.602453,-46.645632&amp;sspn=0.027056,0.049524&amp;gl=br&amp;ie=UTF8&amp;hq=&amp;hnear=R.+Iguatemi,+284+-+Itaim+Bibi,+S%C3%A3o+Paulo,+01451-010&amp;t=m&amp;z=14&amp;ll=-23.583677,-46.682767">Exibir mapa ampliado</a></small>
		</div>

	</div>

	<div id="fale-conosco" class="form-container">
		<h1><span>&raquo;</span> Fale conosco</h1>
		<h2>Clique aqui para enviar-nos uma mensagem.</h2>
		<form name="contato-form" method="post" action="contato/enviar">

			<input type="text" name="nome" placeholder="nome completo" id="input-nome" required value="<?=$this->session->flashdata('contato-nome')?>">

			<input type="email" name="email" placeholder="e-mail" id="input-email" required value="<?=$this->session->flashdata('contato-email')?>">

			<input type="text" name="telefone" placeholder="telefone" id="input-telefone" value="<?=$this->session->flashdata('contato-telefone')?>">

			<textarea name="mensagem" placeholder="mensagem" id="input-mensagem" required><?=$this->session->flashdata('contato-mensagem')?></textarea>

			<input type="submit" value="ENVIAR">

		</form>
	</div>

	<div id="trabalhe-conosco" class="form-container hid">
		<h1><span>&raquo;</span> Trabalhe conosco!</h1>
		<h2>Clique aqui para enviar-nos seu currículo e fazer partedo nosso Banco de Talentos</h2>
		<form name="trabalhe-form" method="post" action="contato/enviarcv" enctype="multipart/form-data">

			<input type="text" name="nome" placeholder="nome completo" id="input-nome" required value="<?=$this->session->flashdata('trabalhe-nome')?>">

			<input type="email" name="email" placeholder="e-mail" id="input-email" required value="<?=$this->session->flashdata('trabalhe-email')?>">

			<input type="text" name="telefone" placeholder="telefone fixo" id="input-telefone" required value="<?=$this->session->flashdata('trabalhe-telefone')?>">

			<input type="text" name="celular" placeholder="telefone celular" id="input-celular" value="<?=$this->session->flashdata('trabalhe-celular')?>">

			<input type="text" name="area_interesse" placeholder="área de atuação / interesse" id="input-area_interesse" required value="<?=$this->session->flashdata('trabalhe-area_interesse')?>">

			<textarea name="observacoes" placeholder="observações ou mini-currículo - se desejado" id="input-observacoes"><?=$this->session->flashdata('trabalhe-observacoes')?></textarea>

			<label id="fake-input">
				<div id="overlay">ANEXAR CURRÍCULO [formato Word ou PDF - até 2 Mb]</div>
				<input type="file" name="userfile" id="input-cv" required>
			</label>

			<input type="submit" value="ENVIAR">

		</form>
	</div>

</div>

<?php if ($this->session->flashdata('mensagem_erro')): ?>
	<script defer>
		$('document').ready( function(){
			alert("<?=$this->session->flashdata('mensagem_erro')?>");
		});
	</script>
<?php endif ?>

<?php if ($this->session->flashdata('mensagem_sucesso_contato')): ?>
	<script defer>
		$('document').ready( function(){
			alert("<?=$this->session->flashdata('mensagem_sucesso_contato')?>");
		});
	</script>
<?php endif ?>

<?php if ($this->session->flashdata('mensagem_sucesso_trabalhe')): ?>
	<script defer>
		$('document').ready( function(){
			alert("<?=$this->session->flashdata('mensagem_sucesso_trabalhe')?>");
		});
	</script>	
<?php endif ?>