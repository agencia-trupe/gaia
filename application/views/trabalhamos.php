<div class="caixa-superior">

	<h1>DESENVOLVIMENTO<br>Organizacional</h1>

	<p>
		O processo de <strong>desenvolvimento organizacional é dinâmico e deve ser reorientado ou ajustado na medida em que novas necessidades sejam identificadas</strong>, sempre em linha com os objetivos da organização. 
	</p>

</div>

<div id="texto-trabalhamos">

	<p>
		Nossa metodologia consiste em envolver as lideranças e colaboradores, assim como na definição das estratégias e ferramentas de implementação e  consolidação, necessárias para a adequação de estruturas e processos coerentes com as novas circunstâncias.
	</p>

	<p class="com-borda">
		<strong>Nossa atuação em consultoria de desenvolvimento organizacional concentra-se nas seguintes atividades:</strong>
	</p>

	<p class="com-borda">
		Apoio às organizações em processos de construção de equipes conscientes e de alta performance 
	</p>

	<p class="com-borda">
		Parceria na governança empresarial e no desenvolvimento da equipe para atingir as metas corporativas 
	</p>

	<p class="com-borda">
		Implementação de programas de desenvolvimento de liderança, team building e gestão de talentos 
	</p>

	<p class="com-borda">
		Alinhamento do planejamento estratégico com o time envolvido no core business da empresa 
	</p>

</div>