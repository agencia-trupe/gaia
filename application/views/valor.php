<div class="caixa-superior">

	<h1>Conheça a<br>GAIA Capital Humano</h1>
	
</div>

<div id="floated-text">
	<p>
		Os trabalhos desenvolvidos pela Gaia Capital Humano trazem uma série de benefícios no curto, médio e longo prazo para as organizações e seus colaboradores. Numa primeira etapa os programas resultam em transformações pontuais e momentâneas, sendo desenvolvidas junto à empresa ao longo do tempo. 
	</p>
	<p>
		Gera diferencial de relacionamento com a governança 
	</p>
	<p>
		Foco em desenvolver o indivíduo e a empresa, provocar reflexões nos dois níveis 
	</p>
	<p>
		Análise do perfil da empresa (cultura) e avaliação se os profissionais estão alinhados e preparados para os desafios 
	</p>
	<p>
		Identificar os grandes obstáculos para que o sucesso daquela empresa aconteça: quais fatores que podem impedir a empresa de crescer? 
	</p>
	<p>
		Quem são as pessoas que fizeram a empresa alcançar aquele patamar e quais farão a empresa avançar em outros níveis? 
	</p>
	<p>
		Como ter um olhar maior e gerar reflexão? 
	</p>
	<p>
		Empresa: Qual a razão de existir? Aonde quer chegar? Ajudar apensar numa linha histórica. Papéis e componentes dessa equação. 
	</p>
	<p>
		Individuo x papel (bussiness x pessoas alocadas). 
	</p>
</div>

<div id="beneficios">

	<div id="quote">
		Mergulhar em algo novo<br>
		Fazer uma experiência<br>
		Transcender os fatos<br>
		Provocar uma vivencia significativa<br>
		Olhar a vida com novas lentes<br>
		Buscar um novo jeito de ser<br>
		Admitir uma nova maneira de comprometer-se<br>
		Sonhar com um nova amanhã<br>
		Viver com sentido<br>
		<span>Canísio Mayer</span>
	</div>

	<div class="text">
		<h2>
			Benefícios da Consultoria Gaia Capital Humano
		</h2>
		<p class="sem-borda">
			Acreditamos que indivíduos realizados e que compreendam de forma plena seu papel nas empresas, contribuam de forma mais eficiente para o desenvolvimento e evolução das organizações. 
		</p>
		<p>
			Atuando como elemento de transformação nas pessoas, empresas e sociedade, podemos destacar os principais benefícios alcançados e percebidos pelas empresas após a implementação de um projeto Gaia Capital Humano: 
		</p>
		<p>
			Apresentam melhores resultados 
		</p>
		<p>
			Tornam-se mais competitivas 
		</p>
		<p>
			Voltam-se conscientemente para as necessidades dos clientes 
		</p>
		<p>
			Alcançam a sinergia entre os colaboradores 
		</p>
		<p>
			Profissionais se tornam mais conscientes sobre o seu papel na organização e comprometidos com a empresa 
		</p>
		<p>
			Passam a trabalhar os conflitos de forma positiva, desenvolvendo espírito de cooperação 
		</p>
		<p>
			Tornam-se mais flexíveis, ousados e criativos, reagindo com prontidão às mudanças do mercado.
		</p>
	</div>

</div>