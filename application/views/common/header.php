<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="pt-BR"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="pt-BR"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="pt-BR"> <!--<![endif]-->
<head>
  <meta charset="utf-8">

  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Gaia Capital Humano</title>
  <meta name="description" content="">
  <meta name="keywords" content="" />
  <meta name="robots" content="index, follow" />
  <meta name="author" content="Trupe Design" />
  <meta name="copyright" content="2012 Trupe Design" />

  <meta name="viewport" content="width=device-width,initial-scale=1">

  <meta property="og:title" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:type" content="website"/>
  <meta property="og:image" content="<?=base_url('_imgs/layout/fb.jpg')?>"/>
  <meta property="og:url" content="<?=base_url()?>"/>
  <meta property="og:description" content=""/>

  <base href="<?= base_url() ?>">
  <script> var BASE = '<?= base_url() ?>'</script>

  <link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>

  <?CSS(array('reset', 'base', 'fontface/stylesheet', $this->router->class, $load_css))?>  
  

  <?if(ENVIRONMENT == 'development'):?>
    
    <?JS(array('modernizr-2.0.6.min', 'less-1.3.0.min', 'jquery-1.8.0.min', $this->router->class, $load_js))?>
    
  <?else:?>

    <?JS(array('modernizr-2.0.6.min', $this->router->class, $load_js))?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/jquery-1.8.0.min.js"><\/script>')</script>

  <?endif;?>

</head>

<!--[if IE ]>
  <body class="ie-body ie-body-<?=$this->router->class?>">
<![endif]-->
<!--[if !IE]>-->
  <body class="body-<?=$this->router->class?>">
<!--<![endif]-->

  <div class="fundo-maior">

    <div class="centro">