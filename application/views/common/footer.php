
        </div> <!-- fim da div main -->

      </div> <!-- fim da div centro -->

    <footer>
      <div class="centro">

        <div class="endereco">
          <strong>11 5581.6766</strong>
          <br>
          <br>
          Rua Maysa Figueira Monjardim, 67 • Vila Clementino<br>
          04042-050 • São Paulo, SP
        </div>

        <div id="assinatura">
          &copy; <?=date('Y')?> Gaia Capital Humano<br>
          Todos os direitos reservados<br>
          <br>
            <a href="http://www.trupe.net" title="Criação de sites: Trupe Agência Criativa" target="_blank"><span>Criação de sites: Trupe Agência Criativa</span> <img src="_imgs/layout/trupe.png" alt="Trupe Agência Criativa"><a>
        </div>

      </div>
    </footer>

  </div> <!-- fim da div fundo-maior -->
  
  
  <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
    <script>
      window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
      Modernizr.load({
        load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
      });
    </script>
  <?endif;?>

  <?JS(array('front'))?>
  
</body>
</html>
