<header>

	<ul id="link-social">
		<li><a href="" target="_blank" title="Twitter" class="twitter">Twitter</a></li>
		<li><a href="" target="_blank" title="Facebook" class="facebook">Facebook</a></li>
		<li><a href="" target="_blank" title="LinkedIn" class="linkedIn">LinkedIn</a></li>
	</ul>

	<img src="_imgs/layout/logo.png" alt="Gaia Capital Humano" id="logo">

	<nav>
		<ul>
			<li><a class="escuro <?if($this->router->class=='home')echo"ativo"?>" href="home" title="Página Inicial" id="mn-home">HOME</a></li>
			<li><a class="escuro <?if($this->router->class=='conheca')echo"ativo"?>" href="conheca-a-gaia" title="Conheça a Gaia" id="mn-conheca">CONHEÇA A GAIA CAPITAL HUMANO</a></li>
			<li><a class="escuro <?if($this->router->class=='trabalhamos')echo"ativo"?>" href="como-trabalhamos" title="Como Trabalhamos" id="mn-trabalhamos">COMO TRABALHAMOS?</a></li>
			<li><a class="medio marged <?if($this->router->class=='fazemos')echo"ativo"?>" href="o-que-fazemos" title="O que Fazemos" id="mn-fazemos">O QUE FAZEMOS?</a></li>
			<li><a class="medio <?if($this->router->class=='nucleos')echo"ativo"?>" href="nucleos-de-trabalho" title="Núcleos de Trabalho" id="mn-nucleos">NÚCLEOS DE TRABALHO</a></li>
			<li><a class="medio <?if($this->router->class=='capital')echo"ativo"?>" href="nosso-capital-humano" title="Nosso Capital Humano" id="mn-capital">NOSSO CAPITAL HUMANO</a></li>
			<li><a class="claro doublemarged <?if($this->router->class=='valor')echo"ativo"?>" href="agregando-valor-ao-capital-humano" title="Agregando Valor" id="mn-valor">AGREGANDO VALOR AO CAPITAL HUMANO</a></li>
			<li><a class="claro <?if($this->router->class=='clientes')echo"ativo"?>" href="clientes" title="Clientes" id="mn-clientes">CLIENTES</a></li>
			<li><a class="claro <?if($this->router->class=='contato')echo"ativo"?>" href="contato" title="Contato" id="mn-contato">CONTATO</a></li>
		</ul>		
	</nav>	
	
</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">

	<div id="spacer"></div>