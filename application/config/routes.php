<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';

$route['conheca-a-gaia'] = "conheca";
$route['conheca-a-gaia/(:any)'] = "conheca/$1";
$route['como-trabalhamos'] = "trabalhamos";
$route['como-trabalhamos/(:any)'] = "trabalhamos/$1";
$route['o-que-fazemos'] = "fazemos";
$route['o-que-fazemos/(:any)'] = "fazemos/$1";
$route['nucleos-de-trabalho'] = "nucleos";
$route['nucleos-de-trabalho/(:any)'] = "nucleos/$1";
$route['nosso-capital-humano'] = "capital";
$route['nosso-capital-humano/(:any)'] = "capital/$1";
$route['agregando-valor-ao-capital-humano'] = "valor";
$route['agregando-valor-ao-capital-humano/(:any)'] = "valor/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */