<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
		$this->load->model('clientes_model', 'model');
	}

	function index(){
		$data['clientes'] = $this->model->pegarTodos();
		$this->load->view('clientes', $data);
	}

}
