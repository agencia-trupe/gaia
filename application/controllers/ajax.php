<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('home');
    }

    function pegarNucleo(){
    	$slug = $this->input->post('slug');
    	$query = $this->db->get_where('nucleos', array('slug' => $slug))->result();
    	echo $query[0]->texto;
    } 

}
