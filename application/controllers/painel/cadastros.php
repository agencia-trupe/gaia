<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastros extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('cadastros_model', 'cadastros');
   }

   function index($pag = 0){
   	$this->load->library('pagination');

      $pag_options = array(
         'base_url' => base_url("painel/cadastros/index/"),
         'per_page' => 20,
         'uri_segment' => 4,
         'next_link' => "Próxima →",
         'next_tag_open' => "<li class='next'>",
         'next_tag_close' => '</li>',
         'prev_link' => "← Anterior",
         'prev_tag_open' => "<li class='prev'>",
         'prev_tag_close' => '</li>',
         'display_pages' => TRUE,
         'num_links' => 10,
         'first_link' => FALSE,
         'last_link' => FALSE,
         'num_tag_open' => '<li>',
         'num_tag_close' => '</li>',
         'cur_tag_open' => '<li><b>',
         'cur_tag_close' => '</b></li>',
         'total_rows' => $this->cadastros->numeroResultados()
      );

      $this->pagination->initialize($pag_options);
      $data['paginacao'] = $this->pagination->create_links();

      $data['registros'] = $this->cadastros->pegarPaginado($pag_options['per_page'], $pag);

      
      
      $data['titulo'] = "Cadastros"; 
      $data['unidade'] = "Relatório de cadastros";
      $data['campo_1'] = "Usuário";
      $data['campo_2'] = "E-mail";

      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;
     
      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
      else
         $data['mostrarsucesso'] = false;

      $this->load->view('painel/cadastros/lista', $data);
   }

   function extrair(){
      $this->layout = FALSE;
      $this->load->dbutil();

      $users = $this->db->order_by('nome', 'asc')->select('nome, email')->get('cadastros');
      $filename = "cadastros_".Date('d-m-Y_H-i-s').'.csv';
      
      $delimiter = ",";
      $newline = "\r\n";
 
      $this->output->set_header('Content-Type: application/force-download');
      $this->output->set_header("Content-Disposition: attachment; filename='$filename'");
      $this->output->set_content_type('text/csv')->set_output($this->dbutil->csv_from_result($users, $delimiter, $newline));      
   }

   function excluir($id){
	   if($this->cadastros->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir usuário');
      }

      redirect('painel/cadastros/index/', 'refresh');
   }

}