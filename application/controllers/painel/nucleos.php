<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nucleos extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Núcleos de Trabalho";
		$this->unidade = "Texto";
		$this->load->model('nucleos_model', 'model');
	}

}