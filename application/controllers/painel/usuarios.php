<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('usuarios_model', 'usuarios');
   }

   function excluir($id){
	   if($this->usuarios->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Usuário excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir usuário');
      }

      if($this->session->userdata('id') == $id)
         redirect('painel/home/logout');

   	redirect('painel/usuarios', 'refresh');
   }

}