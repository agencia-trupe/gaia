<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Capital extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Capital Humano";
		$this->unidade = "Texto";
		$this->load->model('capital_model', 'model');
	}

}