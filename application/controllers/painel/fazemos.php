<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fazemos extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "O que fazemos?";
		$this->unidade = "Texto";
		$this->load->model('fazemos_model', 'model');
	}

}