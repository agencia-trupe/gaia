<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Clientes";
		$this->unidade = "Cliente";
		$this->load->model('clientes_model', 'model');
	}

    function index(){

        $data['registros'] = $this->model->pegarTodos('ordem', 'asc');

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }
}