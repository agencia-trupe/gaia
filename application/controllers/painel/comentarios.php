<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comentarios extends MY_Admincontroller {

	function __construct(){
		parent::__construct();   		
   	}

   	// LANG = pt || es || en
   	// AREA = noticias || giro || mapa
   	function index($lang = 'pt', $area = 'noticias', $id = false){

   		if ($area != 'mapa' && !$id)
   			redirect('painel/home/');

	    if($this->session->flashdata('mostrarerro') === true)
	        $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
	    else
	        $data['mostrarerro'] = false;
	     
	    if($this->session->flashdata('mostrarsucesso') === true)
	        $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
	    else
	        $data['mostrarsucesso'] = false;
   		
   		switch ($area) {
   			case 'noticias':
   				$data['comentarios'] = $this->db->order_by('data', 'desc')->get_where($lang."_noticias_comentarios", array('id_noticia' => $id))->result();
   				$qry_noticia = $this->db->get_where($lang.'_noticias', array('id' => $id))->result();
   				$data['titulo'] = "Comentários da notícia : " . $qry_noticia[0]->titulo;
   				break;
   			case 'giro':
   				$data['comentarios'] = $this->db->order_by('data', 'desc')->get_where($lang."_giro_comentarios", array('id_noticia' => $id))->result();
   				$qry_noticia = $this->db->get_where($lang.'_giro', array('id' => $id))->result();
   				$data['titulo'] = "Comentários do Giro : " . $qry_noticia[0]->titulo;
   				break;
   			case 'mapa':
   				$data['comentarios'] = $this->db->order_by('data', 'desc')->get($lang."_mapa_comentarios")->result();
               
               if ($lang == 'pt')
                  $data['titulo'] = "Comentários em Português do Mapa de Recursos Hídricos";
               elseif($lang == 'es')
                  $data['titulo'] = "Comentários em Espanhol do Mapa de Recursos Hídricos";
               elseif($lang == 'en')
                  $data['titulo'] = "Comentários em Inglês do Mapa de Recursos Hídricos";

   				break;   			
   		}

   		foreach ($data['comentarios'] as $key => $value) {
   			$value->autor = $this->pegarAutorComentario($value->autor);
   		}

   		$data['lang'] = $lang;
   		$data['area'] = $area;
   		$data['id_noti'] = $id;
   		$this->load->view('painel/comentarios/lista', $data);
   	}

	function pegarAutorComentario($id){
		$qry = $this->db->get_where("cadastros_comentarios", array('id' => $id))->result();
		if(isset($qry[0]) && $qry[0])
			return $qry[0];
		else
			return false;
	}

	function excluir($lang = 'pt', $area = 'noticias', $id = false, $id_noti = false){

   		if ($area != 'mapa' && !$id)
   			redirect('painel/home/');
   		
   		switch ($area) {
   			case 'noticias':
   				$excluir = $this->db->where('id', $id)->delete($lang."_noticias_comentarios");
   				break;
   			case 'giro':
   				$excluir = $this->db->where('id', $id)->delete($lang."_giro_comentarios");
   				break;
   			case 'mapa':
   				$excluir = $this->db->where('id', $id)->delete($lang."_mapa_comentarios");
   				break;   			
   		}

	   	if($excluir){
        	$this->session->set_flashdata('mostrarsucesso', true);
        	$this->session->set_flashdata('mostrarsucesso_mensagem', 'Comentário excluido com sucesso');
      	}else{
        	$this->session->set_flashdata('mostrarerro', true);
        	$this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir comentário');
      	}	

   		
   		redirect('painel/comentarios/index/'.$lang.'/'.$area.'/'.$id_noti);
	}
}
?>