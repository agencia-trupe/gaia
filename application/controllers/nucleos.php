<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nucleos extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
		$this->load->model('nucleos_model', 'model');
	}

	function index(){
		$data['registro'] = $this->model->pegarTodos();

		$this->load->view('nucleos', $data);
	}

}
