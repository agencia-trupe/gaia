<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trabalhamos extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
	}

	function index($slug = ''){

		$slugs['authoria'] = <<<STR

STR;
		$slugs['liderancafeminina'] = <<<STR

STR;
		$slugs['consultores'] = <<<STR

STR;
		$slugs['coach'] = <<<STR

STR;
		$slugs['equilibrio'] = <<<STR

STR;
		$slugs['taylormade'] = <<<STR

STR;

		if($slug)
			$data['texto'] = $slugs[$slug];
		else
			$data['texto'] = <<<STR
		<p>
			Além dos processos e atividades relacionados ao <strong>desenvolvimento organizacional</strong> e voltados à transformação do indivíduo no ambiente de trabalho, a <strong>Gaia Capital Humano</strong> especializou-se em alguns grupos de trabalhos (Núcleos), de forma a oferecer soluções ainda mais personalizadas de acordo com o público de cada processo. 
		</p>

		<p>
			<br>
			<br>
			<br>
			<strong>« CLIQUE NOS NÚCLEOS PARA SABER MAIS</strong>
		</p>
STR;

		
		if($this->input->is_ajax_request()){
			$this->hasLayout = FALSE;
			echo $data['texto'];
		}else
			$this->load->view('trabalhamos', $data);
	}

}
