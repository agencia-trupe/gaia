<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fazemos extends MY_Frontcontroller {

	function __construct(){
		parent::__construct();
		$this->load->model('fazemos_model', 'model');
	}

	function index(){
		$data['registro'] = $this->model->pegarTodos();

		$this->load->view('fazemos', $data);
	}

}
