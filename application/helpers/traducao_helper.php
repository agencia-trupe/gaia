<?php

function traduz($label, $ajax = FALSE){
    
    $CI =& get_instance();

    if($ajax)
        $CI->lang->load($CI->session->userdata('language').'_site', $CI->session->userdata('language'));
    
    $return = $CI->lang->line($label);
    
    if($return)
        return $return;
    else
        //return prefixo(str_repeat('non', (strlen($label) - 3) / 3));
        return $label;
}

/*
 * Função para adicionar o prefixo definido na sessão de acordo com a linguagem
 */
function prefixo($arg){
    $CI =& get_instance();
    return $CI->session->userdata('prefixo').$arg;
}

?>